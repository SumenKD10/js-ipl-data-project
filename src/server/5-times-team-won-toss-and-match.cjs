const csv = require("csv-parser");
const fs = require("fs");

function times_Team_Won_Toss_And_Match() {
    const matchDetails = [];
    const dataPath = __dirname + "/../data/matches.csv";
    const resultPath = __dirname + "/../public/output/5-times-team-won-toss-and-match.json";


    //data toss_winner and winner
    fs.createReadStream(dataPath)
        .pipe(csv())
        .on("data", (data) => matchDetails.push(data))
        .on("end", () => {
            let allTeamResult = matchDetails.reduce((allTeam, eachMatch) => {
                if (eachMatch.winner === eachMatch.toss_winner) {
                    if (allTeam[eachMatch.toss_winner] !== undefined) {
                        allTeam[eachMatch.toss_winner] += 1;
                    } else {
                        allTeam[eachMatch.toss_winner] = 1;
                    }
                }
                return allTeam;
            }, {});

            const jsonString = JSON.stringify(allTeamResult);
            fs.writeFile(resultPath, jsonString, (errorFound) => {
                if (errorFound) {
                    console.log("Error in writing file", errorFound);
                } else {
                    console.log("Successfully written inside file");
                }
            });
        });
}

times_Team_Won_Toss_And_Match();