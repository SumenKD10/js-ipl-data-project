const csv = require("csv-parser");
const fs = require("fs");


function matches_Per_Year() {
    const matchDetails = [];
    const dataPath = __dirname + "/../data/matches.csv";
    const resultPath = __dirname + "/../public/output/1-matches-per-year.json";

    fs.createReadStream(dataPath)
        .pipe(csv())
        .on("data", (data) => matchDetails.push(data))
        .on("end", () => {
            let matchesPerYear = matchDetails.reduce((matchesPerYearResult, currentMatche) => {
                if (matchesPerYearResult[currentMatche.season]) {
                    matchesPerYearResult[currentMatche.season] += 1;
                } else {
                    matchesPerYearResult[currentMatche.season] = 1;
                }
                return matchesPerYearResult;
            }, {});

            const jsonString = JSON.stringify(matchesPerYear);
            fs.writeFile(resultPath, jsonString, (errorFound) => {
                if (errorFound) {
                    console.log("Error in writing file", errorFound);
                } else {
                    console.log("Successfully written inside file");
                }
            });
        });
}


matches_Per_Year();