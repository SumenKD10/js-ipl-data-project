const csv = require("csv-parser");
const fs = require("fs");

function player_Of_The_Match() {
    const matchDetails = [];
    const dataPath = __dirname + "/../data/matches.csv";
    const resultPath = __dirname + "/../public/output/6-player-of-the-match.json";


    //data player_of_match, season
    fs.createReadStream(dataPath)
        .pipe(csv())
        .on("data", (data) => matchDetails.push(data))
        .on("end", () => {
            let count = 1;
            let allTeamResult = matchDetails.reduce((allTeam, eachMatch) => {
                if (allTeam[eachMatch.season] !== undefined) {
                    if (allTeam[eachMatch.season][eachMatch.player_of_match] !== undefined) {
                        allTeam[eachMatch.season][eachMatch.player_of_match] += 1;   //frequency
                    } else {
                        allTeam[eachMatch.season][eachMatch.player_of_match] = 1;  //frequency
                    }
                } else {
                    allTeam[eachMatch.season] = {};
                    allTeam[eachMatch.season][eachMatch.player_of_match] = 1; //frequency
                }
                return allTeam;
            }, {});

            // Got all the player of match titles for each season in last object.

            let finalObjectWithHighestWonPlayer = {};
            for (let property in allTeamResult) {
                let eachSeasonArray = Object.entries(allTeamResult[property]);
                //Now Sorting each object inside according to second element(frequency)
                eachSeasonArray.sort(function (firstValue, secondValue) {
                    const firstFrequency = Number(firstValue[1]);
                    const secondFrequency = Number(secondValue[1]);
                    if (firstFrequency > secondFrequency) {
                        return -1;
                    } else {
                        return 1;
                    }
                });
                finalObjectWithHighestWonPlayer[property] = Object.fromEntries([eachSeasonArray[0]]); //Getting Players with highest frequency for each year.
            }


            const jsonString = JSON.stringify(finalObjectWithHighestWonPlayer);
            fs.writeFile(resultPath, jsonString, (errorFound) => {
                if (errorFound) {
                    console.log("Error in writing file", errorFound);
                } else {
                    console.log("Successfully written inside file");
                }
            });
        });
}

player_Of_The_Match();