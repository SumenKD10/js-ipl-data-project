const csv = require("csv-parser");
const fs = require("fs");

function extra_Runs_Conceded() {
    const matchDetails = [];
    const deliveryDetails = [];
    const dataPath1 = __dirname + "/../data/matches.csv";
    const dataPath2 = __dirname + "/../data/deliveries.csv";
    const resultPath = __dirname + "/../public/output/3-extra-runs-conceded-per-team-2016.json";

    fs.createReadStream(dataPath1)
        .pipe(csv())
        .on("data", (data) => matchDetails.push(data))
        .on("end", () => {
            let allMatchIDsResult = matchDetails.filter((allMatchesID) => {
                return allMatchesID.season === '2016';
            })
                .map((match) => {
                    return match.id
                });


            fs.createReadStream(dataPath2)
                .pipe(csv())
                .on("data", (data) => deliveryDetails.push(data))
                .on("end", () => {
                    let allTeamResult = deliveryDetails.reduce((allTeamsExtraRun, eachMatch) => {
                        if (allMatchIDsResult.includes(eachMatch.match_id)) {
                            if (allTeamsExtraRun[eachMatch.bowling_team]) {
                                allTeamsExtraRun[eachMatch.bowling_team] += Number(eachMatch.extra_runs);
                            } else {
                                allTeamsExtraRun[eachMatch.bowling_team] = Number(eachMatch.extra_runs);
                            }
                        }
                        return allTeamsExtraRun;
                    }, {});

                    const jsonString = JSON.stringify(allTeamResult);
                    fs.writeFile(resultPath, jsonString, (errorFound) => {
                        if (errorFound) {
                            console.log("Error in writing file", errorFound);
                        } else {
                            console.log("Successfully written inside file");
                        }
                    });
                });
        });
}
extra_Runs_Conceded();