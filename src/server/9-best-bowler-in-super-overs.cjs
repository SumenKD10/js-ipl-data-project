const csv = require("csv-parser");
const fs = require("fs");

function best_bowler_economy() {
    const matchDetails = [];
    const deliveryDetails = [];
    const dataPath = __dirname + "/../data/deliveries.csv";
    const resultPath = __dirname + "/../public/output/9-best-bowler-in-super-overs.json";

    // data is_super_over
    fs.createReadStream(dataPath)
        .pipe(csv())
        .on("data", (data) => deliveryDetails.push(data))
        .on("end", () => {
            let bestBowlersObject = deliveryDetails.filter((dataFound) => {
                return (dataFound.is_super_over === '1');
            })
                .reduce((bowlersAndTheirBalls, eachDelivery) => {
                    let checkValuesForWideAndNoBall = 0;
                    if ((Number(eachDelivery.wide_runs) === 0 && Number(eachDelivery.noball_runs) === 0)) {
                        checkValuesForWideAndNoBall = 1;
                    }
                    if (bowlersAndTheirBalls[eachDelivery.bowler] === undefined) {
                        bowlersAndTheirBalls[eachDelivery.bowler] = Number(eachDelivery.total_runs);     //Runs Conceded
                        bowlersAndTheirBalls[eachDelivery.bowler + ' balls'] = checkValuesForWideAndNoBall ? 1 : 0;    //Balls Bowled
                    } else {
                        bowlersAndTheirBalls[eachDelivery.bowler] += Number(eachDelivery.total_runs);    //Runs Conceded
                        bowlersAndTheirBalls[eachDelivery.bowler + " balls"] += checkValuesForWideAndNoBall ? 1 : 0;   //Balls Bowled
                    }
                    return bowlersAndTheirBalls;
                }, {});

            let bestBowlersObjectArray = [];
            let count = 1;       // To get every odd number objects(Runs Conceded by each Bowler) available inside the bowlersAndTheirBallsResult Object
            for (let property in bestBowlersObject) {
                if (count % 2 === 1) {
                    let ballsBowled = property + ' balls';    //Balls Bowled
                    let runsConceded = property;               //Runs Conceded
                    let economyRate = (bestBowlersObject[runsConceded] / (bestBowlersObject[ballsBowled] / 6)).toFixed(2);    // (Runs Conceded)/(total Overs) and fixing it to decimal 2.
                    bestBowlersObjectArray.push([property, economyRate]);
                }
                count++;
            }

            //Sorting the Array we Got.
            bestBowlersObjectArray.sort(function (firstValue, secondValue) {
                const firstValueEconomyRate = Number(firstValue[1]);
                const secondValueEconomyRate = Number(secondValue[1]);
                if (firstValueEconomyRate < secondValueEconomyRate) {
                    return -1;
                } else {
                    return 1;
                }
            });

            //Slicing the Array and Putting into the Object
            let answerObject = Object.fromEntries(bestBowlersObjectArray.slice(0, 1));

            const jsonString = JSON.stringify(answerObject);
            fs.writeFile(resultPath, jsonString, (errorFound) => {
                if (errorFound) {
                    console.log("Error in writing file", errorFound);
                } else {
                    console.log("Successfully written inside file");
                }
            });
        });
}
best_bowler_economy();