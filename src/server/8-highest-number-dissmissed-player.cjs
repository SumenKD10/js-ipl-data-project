const csv = require("csv-parser");
const fs = require("fs");

function highest_dissmissed_player() {
    const deliveryDetails = [];
    const dataPath = __dirname + "/../data/deliveries.csv";
    const resultPath = __dirname + "/../public/output/8-highest-number-dissmissed-player.json";


    //data player_dismissed , batsman, bowler, dismissal_kind
    fs.createReadStream(dataPath)
        .pipe(csv())
        .on("data", (data) => deliveryDetails.push(data))
        .on("end", () => {
            let allBatsmanResult = deliveryDetails.filter((dataFound) => {
                return (dataFound.player_dismissed === dataFound.batsman && dataFound.dismissal_kind !== 'run out');
            })
                .reduce((allBatsman, eachDelivery) => {
                    if (allBatsman[eachDelivery.batsman] !== undefined) {
                        if (allBatsman[eachDelivery.batsman][eachDelivery.bowler] !== undefined) {
                            allBatsman[eachDelivery.batsman][eachDelivery.bowler] += 1;
                        } else {
                            allBatsman[eachDelivery.batsman][eachDelivery.bowler] = 1;
                        }
                    } else {
                        allBatsman[eachDelivery.batsman] = {};
                        allBatsman[eachDelivery.batsman][eachDelivery.bowler] = 1;
                    }
                    return allBatsman;
                }, {});

            //Getting the player with maximum dissmissals by another player
            let dissmissedTimes = 0;
            let batsmanValue = "";
            let bowlerValue = "";
            for (let property in allBatsmanResult) {
                for (let propertyInside in allBatsmanResult[property]) {
                    if (dissmissedTimes < allBatsmanResult[property][propertyInside]) {  //Storing data when found maximum dissmissals
                        dissmissedTimes = allBatsmanResult[property][propertyInside];
                        batsmanValue = property;
                        bowlerValue = propertyInside;
                    }
                }
            }

            let answerObject = { answer: `${batsmanValue} has been dissmissed by ${bowlerValue} ${dissmissedTimes} times.` };

            const jsonString = JSON.stringify(answerObject);
            fs.writeFile(resultPath, jsonString, (errorFound) => {
                if (errorFound) {
                    console.log("Error in writing file", errorFound);
                } else {
                    console.log("Successfully written inside file");
                }
            });
        });
}

highest_dissmissed_player();