const csv = require("csv-parser");
const fs = require("fs");

function top_10_Economical_Player() {
    const matchDetails = [];
    const deliveryDetails = [];
    const dataPath1 = __dirname + "/../data/matches.csv";
    const dataPath2 = __dirname + "/../data/deliveries.csv";
    const resultPath = __dirname + "/../public/output/4-top-10-economical-bowler-2015.json";
    const year = '2015';

    fs.createReadStream(dataPath1)
        .pipe(csv())
        .on("data", (data) => matchDetails.push(data))
        .on("end", () => {
            let allMatchIDsResult = matchDetails.filter((allMatchesID) => {
                return allMatchesID.season === year;
            })
                .map((match) => {
                    return match.id;
                });

            //We got all the match Ids for year 2015

            fs.createReadStream(dataPath2)
                .pipe(csv())
                .on("data", (data) => deliveryDetails.push(data))
                .on("end", () => {
                    let bowlersAndTheirBallsResult = deliveryDetails.reduce((bowlersAndTheirBalls, eachDelivery) => {
                        let checkValuesForWideAndNoBall = 0;
                        if ((Number(eachDelivery.wide_runs) === 0 && Number(eachDelivery.noball_runs) === 0)) {
                            checkValuesForWideAndNoBall = 1;
                        }
                        if (allMatchIDsResult.includes(eachDelivery.match_id)) {
                            if (bowlersAndTheirBalls[eachDelivery.bowler] === undefined) {
                                bowlersAndTheirBalls[eachDelivery.bowler] = Number(eachDelivery.total_runs);     //Runs Conceded
                                bowlersAndTheirBalls[eachDelivery.bowler + ' balls'] = checkValuesForWideAndNoBall ? 1 : 0;    //Balls Bowled
                            } else {
                                bowlersAndTheirBalls[eachDelivery.bowler] += Number(eachDelivery.total_runs);    //Runs Conceded
                                bowlersAndTheirBalls[eachDelivery.bowler + " balls"] += checkValuesForWideAndNoBall ? 1 : 0;   //Balls Bowled
                            }
                        }
                        return bowlersAndTheirBalls;
                    }, {});

                    //We got the object with RunsConceded and Balls Bowled by each Bowler.

                    let bowlersAndTheirBallsArray = [];
                    let count = 1;       // To get every odd number objects(Runs Conceded by each Bowler) available inside the bowlersAndTheirBallsResult Object
                    for (let property in bowlersAndTheirBallsResult) {
                        if (count % 2 === 1) {
                            let ballsBowled = property + ' balls';    //Balls Bowled
                            let runsConceded = property;               //Runs Conceded
                            let economyRate = (bowlersAndTheirBallsResult[runsConceded] / (bowlersAndTheirBallsResult[ballsBowled] / 6)).toFixed(2);    // (Runs Conceded)/(total Overs) and fixing it to decimal 2.
                            bowlersAndTheirBallsArray.push([property, economyRate]);
                        }
                        count++;
                    }

                    //Sorting the Array we Got.
                    bowlersAndTheirBallsArray.sort(function (firstValue, secondValue) {
                        const firstValueEconomyRate = Number(firstValue[1]);
                        const secondValueEconomyRate = Number(secondValue[1]);
                        if (firstValueEconomyRate < secondValueEconomyRate) {
                            return -1;
                        } else {
                            return 1;
                        }
                    });

                    //Slicing the Array and Putting into the Object
                    let answerObject = Object.fromEntries(bowlersAndTheirBallsArray.slice(0, 10)); // Putting it inside the object after slicing the array.


                    const jsonString = JSON.stringify(answerObject);
                    fs.writeFile(resultPath, jsonString, (errorFound) => {
                        if (errorFound) {
                            console.log("Error in writing file", errorFound);
                        } else {
                            console.log("Successfully written inside file");
                        }
                    });
                });
        });
}
top_10_Economical_Player();