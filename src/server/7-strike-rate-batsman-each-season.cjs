const csv = require("csv-parser");
const fs = require("fs");

function strike_Rate_Batsman() {
    const matchDetails = [];
    const deliveryDetails = [];
    const dataPath1 = __dirname + "/../data/matches.csv";
    const dataPath2 = __dirname + "/../data/deliveries.csv";
    const resultPath = __dirname + "/../public/output/7-strike-rate-batsman-each-season.json";

    //data total_runs
    fs.createReadStream(dataPath1)
        .pipe(csv())
        .on("data", (data) => matchDetails.push(data))
        .on("end", () => {
            let idSeasonLink = matchDetails.reduce((idSeasonGot, eachMatch) => {
                idSeasonGot[eachMatch.id] = eachMatch.season;
                return idSeasonGot;
            }, {});

            //Made an Object above to link id with season and use it below.
            fs.createReadStream(dataPath2)
                .pipe(csv())
                .on("data", (data) => deliveryDetails.push(data))
                .on("end", () => {
                    let calculatedRunsAndBalls = deliveryDetails.reduce((calRunsAndBalls, eachDelivery) => {
                        let season = idSeasonLink[eachDelivery.match_id];
                        if (calRunsAndBalls[eachDelivery.batsman] !== undefined) {
                            if (calRunsAndBalls[eachDelivery.batsman][season] !== undefined) {
                                calRunsAndBalls[eachDelivery.batsman][season]['runs'] += Number(eachDelivery.total_runs);
                                calRunsAndBalls[eachDelivery.batsman][season]['balls'] += (Number(eachDelivery.wide_runs) === 0) ? 1 : 0;
                            } else {
                                calRunsAndBalls[eachDelivery.batsman][season] = {};
                                calRunsAndBalls[eachDelivery.batsman][season]['runs'] = Number(eachDelivery.total_runs);
                                calRunsAndBalls[eachDelivery.batsman][season]['balls'] = (Number(eachDelivery.wide_runs) === 0) ? 1 : 0;
                            }
                        } else {
                            calRunsAndBalls[eachDelivery.batsman] = {};
                            calRunsAndBalls[eachDelivery.batsman][season] = {};
                            calRunsAndBalls[eachDelivery.batsman][season]['runs'] = Number(eachDelivery.total_runs);
                            calRunsAndBalls[eachDelivery.batsman][season]['balls'] = (Number(eachDelivery.wide_runs) === 0) ? 1 : 0;
                        }
                        return calRunsAndBalls;
                    }, {});

                    let calculatedStrikerRate = {};
                    for (let property in calculatedRunsAndBalls) {
                        calculatedStrikerRate[property] = calculatedRunsAndBalls[property];
                        for (let propertyInside in calculatedStrikerRate[property]) {
                            let totalRuns = calculatedRunsAndBalls[property][propertyInside].runs;
                            let ballsBowled = calculatedRunsAndBalls[property][propertyInside].balls;
                            calculatedStrikerRate[property][propertyInside] = (totalRuns / ballsBowled) * 100;  //calculating strike rate
                        }
                    }

                    const jsonString = JSON.stringify(calculatedStrikerRate);
                    fs.writeFile(resultPath, jsonString, (errorFound) => {
                        if (errorFound) {
                            console.log("Error in writing file", errorFound);
                        } else {
                            console.log("Successfully written inside file");
                        }
                    });
                });
        });
}

strike_Rate_Batsman();