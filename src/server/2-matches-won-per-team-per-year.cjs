const csv = require("csv-parser");
const fs = require("fs");

function matches_Won_Per_Team_Per_Year() {
    const matchDetails = [];
    const dataPath = __dirname + "/../data/matches.csv";
    const resultPath = __dirname + "/../public/output/2-matches-won-per-team-per-year.json";

    fs.createReadStream(dataPath)
        .pipe(csv())
        .on("data", (data) => matchDetails.push(data))
        .on("end", () => {
            let matchesWonPerTeamPerYear = matchDetails.reduce((matchesWonPerTeam, currentValue) => {
                if (!matchesWonPerTeam[currentValue.season]) {
                    matchesWonPerTeam[currentValue.season] = {};
                }
                if (!matchesWonPerTeam[currentValue.season][currentValue.winner]) {
                    matchesWonPerTeam[currentValue.season][currentValue.winner] = 1;
                } else {
                    matchesWonPerTeam[currentValue.season][currentValue.winner] += 1;
                }
                return matchesWonPerTeam;
            }, {});

            const jsonString = JSON.stringify(matchesWonPerTeamPerYear);
            fs.writeFile(resultPath, jsonString, (errorFound) => {
                if (errorFound) {
                    console.log("Error in writing file", errorFound);
                } else {
                    console.log("Successfully written inside file");
                }
            });
        });
}

matches_Won_Per_Team_Per_Year();