fetch("output/1-matches-per-year.json")
  .then((data) => data.json())
  .then((data) => {
    const dataToPlot = Object.values(data);
    //console.log("Data Loaded ", dataToPlot);

    Highcharts.chart("container1", {
      title: {
        text: "IPL Matches Played Per Year",
        align: "left",
      },

      subtitle: {
        text: "",
        align: "left",
      },

      yAxis: {
        title: {
          text: "Number of Matches",
        },
      },

      xAxis: {
        title: {
          text: "Year",
        },
        accessibility: {
          rangeDescription: "Range: 2010 to 2020",
        },
      },

      legend: {
        layout: "vertical",
        align: "right",
        verticalAlign: "middle",
      },

      plotOptions: {
        series: {
          label: {
            connectorAllowed: false,
          },
          pointStart: 2008,
        },
      },

      series: [
        {
          name: "Matches",
          data: dataToPlot,
        },
      ],

      responsive: {
        rules: [
          {
            condition: {
              maxWidth: 500,
            },
            chartOptions: {
              legend: {
                layout: "horizontal",
                align: "center",
                verticalAlign: "bottom",
              },
            },
          },
        ],
      },
    });
  });

fetch("output/2-matches-won-per-team-per-year.json")
  .then((data) => data.json())
  .then((data) => {
    let dataToPlot = data;
    let years = Object.keys(dataToPlot);
    let allTeamData = Object.entries(dataToPlot);

    let matchesWonPerTeamPerYear = {};

    allTeamData.map((eachData) => {
      let teamInfo = Object.entries(eachData[1]);

      teamInfo.map((eachElement) => {
        if (matchesWonPerTeamPerYear[eachElement[0]] !== undefined) {
          matchesWonPerTeamPerYear[eachElement[0]][years.indexOf(eachData[0])] =
            eachElement[1];
        } else {
          matchesWonPerTeamPerYear[eachElement[0]] = [];
          matchesWonPerTeamPerYear[eachElement[0]].length =
            Object.keys(dataToPlot).length;
          matchesWonPerTeamPerYear[eachElement[0]].fill(0);

          matchesWonPerTeamPerYear[eachElement[0]][years.indexOf(eachData[0])] =
            eachElement[1];
        }
      });
    });
    // console.log(matchesWonPerTeamPerYear);
    let finalData = [];

    Object.keys(matchesWonPerTeamPerYear).map((eachTeam) => {
      let eachElement = {};
      eachElement["name"] = eachTeam;
      eachElement["data"] = matchesWonPerTeamPerYear[eachTeam];
      finalData.push(eachElement);
    });

    // console.log(finalData);
    Highcharts.chart("container2", {
      chart: {
        type: "column",
      },
      title: {
        text: "Matches Won Per Team Per Year",
        align: "left",
      },
      subtitle: {
        text: "",
      },
      xAxis: {
        categories: Object.keys(dataToPlot),
        crosshair: true,
      },
      yAxis: {
        min: 0,
        title: {
          text: "Number of Matches Won",
        },
      },
      tooltip: {
        headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
        pointFormat:
          '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
          '<td style="padding:0"><b>{point.y:.1f}</b></td></tr>',
        footerFormat: "</table>",
        shared: true,
        useHTML: true,
      },
      plotOptions: {
        column: {
          pointPadding: 0.2,
          borderWidth: 0,
        },
      },
      series: finalData,
    });
  });

fetch("output/3-extra-runs-conceded-per-team-2016.json")
  .then((data) => data.json())
  .then((data) => {
    const dataToPlot = data;
    //console.log("Data Loaded ", dataToPlot);
    //console.log(Object.keys(dataToPlot));
    // console.log(Object.entries(dataToPlot));
    // let newArray = []
    // let getFunction = function () {
    //     Object.keys(dataToPlot).map((each)=>{
    //         newArray.push({
    //             name: each,
    //             data: dataToPlot[each]
    //         });
    //     });
    //     return newArray;
    // };
    // console.log(newArray);
    Highcharts.chart("container3", {
      chart: {
        type: "column",
      },
      title: {
        text: "Extra Runs Conceded Per Team in 2016",
        align: "left",
      },
      subtitle: {
        text: "",
      },
      xAxis: {
        type: "category",
        labels: {
          rotation: -45,
          style: {
            fontSize: "13px",
            fontFamily: "Verdana, sans-serif",
          },
        },
      },
      yAxis: {
        min: 0,
        title: {
          text: "Runs",
        },
      },
      legend: {
        enabled: false,
      },
      tooltip: {
        pointFormat: "Runs By Each Team",
      },
      series: [
        {
          name: "Population",
          data: Object.entries(dataToPlot),
          dataLabels: {
            enabled: true,
            rotation: -90,
            color: "#FFFFFF",
            align: "right",
            format: "{point.y:.1f}", // one decimal
            y: 10, // 10 pixels down from the top
            style: {
              fontSize: "13px",
              fontFamily: "Verdana, sans-serif",
            },
          },
        },
      ],
    });
  });

fetch("output/4-top-10-economical-bowler-2015.json")
  .then((data) => data.json())
  .then((data) => {
    const dataToPlot = data;
    Object.entries(dataToPlot).map((eachElement) => {
      dataToPlot[eachElement[0]] = Number(dataToPlot[eachElement[0]]);
    });
    Highcharts.chart("container4", {
      chart: {
        type: "column",
      },
      title: {
        text: "Top 10 Economical Bowlers in 2015",
        align: "left",
      },
      subtitle: {
        text: "",
      },
      xAxis: {
        type: "category",
        labels: {
          rotation: -45,
          style: {
            fontSize: "13px",
            fontFamily: "Verdana, sans-serif",
          },
        },
      },
      yAxis: {
        min: 0,
        title: {
          text: "Economy Rate",
        },
      },
      legend: {
        enabled: false,
      },
      tooltip: {
        pointFormat: "Economy Rate of Each Bowler",
      },
      series: [
        {
          name: "Population",
          data: Object.entries(dataToPlot),
          dataLabels: {
            enabled: true,
            rotation: -90,
            color: "#FFFFFF",
            align: "right",
            format: "{point.y:.1f}", // one decimal
            y: 10, // 10 pixels down from the top
            style: {
              fontSize: "13px",
              fontFamily: "Verdana, sans-serif",
            },
          },
        },
      ],
    });
  });

fetch("output/5-times-team-won-toss-and-match.json")
  .then((data) => data.json())
  .then((data) => {
    const dataToPlot = data;
    //console.log(Object.entries(dataToPlot));
    Highcharts.chart("container5", {
      chart: {
        type: "column",
      },
      title: {
        text: "Number of Times a Team won both Toss and Match",
        align: "left",
      },
      subtitle: {
        text: "",
      },
      xAxis: {
        type: "category",
        labels: {
          rotation: -45,
          style: {
            fontSize: "13px",
            fontFamily: "Verdana, sans-serif",
          },
        },
      },
      yAxis: {
        min: 0,
        title: {
          text: "Number of Times",
        },
      },
      legend: {
        enabled: false,
      },
      tooltip: {
        pointFormat: "Times",
      },
      series: [
        {
          name: "Population",
          data: Object.entries(dataToPlot),
          dataLabels: {
            enabled: true,
            rotation: -90,
            color: "#FFFFFF",
            align: "right",
            format: "{point.y:.1f}", // one decimal
            y: 10, // 10 pixels down from the top
            style: {
              fontSize: "13px",
              fontFamily: "Verdana, sans-serif",
            },
          },
        },
      ],
    });
  });
fetch("output/6-player-of-the-match.json")
  .then((data) => data.json())
  .then((data) => {
    let dataToPlot = data;
    let years = Object.keys(dataToPlot);
    let allPlayerData = Object.entries(dataToPlot);

    let playerOfTheMatchPerYear = {};

    allPlayerData.map((eachData) => {
      let playerInfo = Object.entries(eachData[1]);

      playerInfo.map((eachElement) => {
        if (playerOfTheMatchPerYear[eachElement[0]] !== undefined) {
          playerOfTheMatchPerYear[eachElement[0]][years.indexOf(eachData[0])] =
            eachElement[1];
        } else {
          playerOfTheMatchPerYear[eachElement[0]] = [];
          playerOfTheMatchPerYear[eachElement[0]].length =
            Object.keys(dataToPlot).length;
          playerOfTheMatchPerYear[eachElement[0]].fill(0);

          playerOfTheMatchPerYear[eachElement[0]][years.indexOf(eachData[0])] =
            eachElement[1];
        }
      });
    });
    // console.log(playerOfTheMatchPerYear);
    let finalData = [];

    Object.keys(playerOfTheMatchPerYear).map((eachPlayer) => {
      let eachElement = {};
      eachElement["name"] = eachPlayer;
      eachElement["data"] = playerOfTheMatchPerYear[eachPlayer];
      finalData.push(eachElement);
    });

    console.log(finalData);
    Highcharts.chart("container6", {
      chart: {
        type: "column",
      },
      title: {
        text: "Player of the Match Each Year",
        align: "left",
      },
      subtitle: {
        text: "",
      },
      xAxis: {
        categories: Object.keys(dataToPlot),
        crosshair: true,
      },
      yAxis: {
        min: 0,
        title: {
          text: "No. of Times Titles Recieved",
        },
      },
      tooltip: {
        headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
        pointFormat:
          '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
          '<td style="padding:0"><b>{point.y:.1f}</b></td></tr>',
        footerFormat: "</table>",
        shared: true,
        useHTML: true,
      },
      plotOptions: {
        column: {
          pointPadding: 0.2,
          borderWidth: 0,
        },
      },
      series: finalData,
    });
  });

fetch("output/8-highest-number-dissmissed-player.json")
  .then((data) => data.json())
  .then((data) => {
    let dataToPlot = Object.values(data);
    const abc = Number(dataToPlot[0].split(' ')[8]);
    let aaa = dataToPlot[0].replace("7 ", "");
    aaa = aaa.replace("times", "");

    const showData = [
        [aaa, abc]
    ];
    // console.log(dataToPlot)
    console.log(aaa);
    // console.log(dataToPlot);
    // console.log(abc)
    //console.log("Data Loaded ", dataToPlot);
    Highcharts.chart("container8", {
      chart: {
        type: "column",
      },
      title: {
        text: "Number of Times a Team won both Toss and Match",
        align: "left",
      },
      subtitle: {
        text: "",
      },
      xAxis: {
        type: "category",
        labels: {
          rotation: 0,
          style: {
            fontSize: "13px",
            fontFamily: "Verdana, sans-serif",
          },
        },
      },
      yAxis: {
        min: 0,
        title: {
          text: "Number of Times",
        },
      },
      legend: {
        enabled: false,
      },
      tooltip: {
        pointFormat: "Times",
      },
      series: [
        {
          name: "Population",
          data: showData,
          dataLabels: {
            enabled: true,
            rotation: 0,
            color: "#FFFFFF",
            align: "center",
            format: "{point.y:.1f}", // one decimal
            y: 10, // 10 pixels down from the top
            style: {
              fontSize: "13px",
              fontFamily: "Verdana, sans-serif",
            },
          },
        },
      ],
    });
  });
fetch("output/9-best-bowler-in-super-overs.json")
  .then((data) => data.json())
  .then((data) => {
    const dataToPlot = Object.values(data);
    //console.log("Data Loaded ", dataToPlot);
    Object.entries(dataToPlot).map((eachElement) => {
      dataToPlot[eachElement[0]] = Number(dataToPlot[eachElement[0]]);
    });
    Highcharts.chart("container9", {
      chart: {
        type: "column",
      },
      title: {
        text: "Most Economical Bowler in Super Over",
        align: "left",
      },
      subtitle: {
        text: "",
      },
      xAxis: {
        type: "category",
        labels: {
          rotation: -45,
          style: {
            fontSize: "13px",
            fontFamily: "Verdana, sans-serif",
          },
        },
      },
      yAxis: {
        title: {
          text: "Economy Rate",
        },
      },
      legend: {
        enabled: false,
      },
      tooltip: {
        pointFormat: "Rate",
      },
      series: [
        {
          name: "Economy Rate",
          data: Object.entries(dataToPlot),
          dataLabels: {
            enabled: true,
            rotation: -90,
            color: "#FFFFFF",
            align: "right",
            format: "{point.y:.1f}", // one decimal
            y: 10, // 10 pixels down from the top
            style: {
              fontSize: "13px",
              fontFamily: "Verdana, sans-serif",
            },
          },
        },
      ],
    });
  });

fetch("output/7-strike-rate-batsman-each-season.json")
  .then((data) => data.json())
  .then((data) => {
    let dataToPlot = data;
    let years = Object.keys(dataToPlot);
    let allTeamData = Object.entries(dataToPlot);

    let matchesWonPerTeamPerYear = {};

    allTeamData.map((eachData) => {
      let teamInfo = Object.entries(eachData[1]);

      teamInfo.map((eachElement) => {
        if (matchesWonPerTeamPerYear[eachElement[0]] !== undefined) {
          matchesWonPerTeamPerYear[eachElement[0]][years.indexOf(eachData[0])] =
            eachElement[1];
        } else {
          matchesWonPerTeamPerYear[eachElement[0]] = [];
          matchesWonPerTeamPerYear[eachElement[0]].length =
            Object.keys(dataToPlot).length;
          matchesWonPerTeamPerYear[eachElement[0]].fill(0);

          matchesWonPerTeamPerYear[eachElement[0]][years.indexOf(eachData[0])] =
            eachElement[1];
        }
      });
    });
    // console.log(matchesWonPerTeamPerYear);
    let finalData = [];

    Object.keys(matchesWonPerTeamPerYear).map((eachTeam) => {
      let eachElement = {};
      eachElement["name"] = eachTeam;
      eachElement["data"] = matchesWonPerTeamPerYear[eachTeam];
      finalData.push(eachElement);
    });

    // console.log(finalData);
    Highcharts.chart("container7", {
      chart: {
        type: "column",
      },
      title: {
        text: "Strike Rate Per Batsman Each Season",
        align: "left",
      },
      subtitle: {
        text: "",
      },
      xAxis: {
        categories: Object.keys(dataToPlot),
        crosshair: true,
      },
      yAxis: {
        min: 0,
        title: {
          text: "Strike Rate",
        },
      },
      tooltip: {
        headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
        pointFormat:
          '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
          '<td style="padding:0"><b>{point.y:.1f}</b></td></tr>',
        footerFormat: "</table>",
        shared: true,
        useHTML: true,
      },
      plotOptions: {
        column: {
          pointPadding: 0.2,
          borderWidth: 0,
        },
      },
      series: finalData,
    });
  });
